# Test package with multiple versions, see its Git tags and branches
module nitpm_test_versions

# Active version of `nitpm_test_versions`
fun version: String do return "2.0"
